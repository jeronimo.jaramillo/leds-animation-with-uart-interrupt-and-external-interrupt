LEDs Animations with interrupts 

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 

Design LEDs animations using six GPIO like digital output. And implement the User button and the UART
to change the modes of speed movement (Using the interrupts).

PLATFORM.
The board STM32F429ZI discovery 1 will be used on this development. 

FUNCTIONALITY.

* The system has the next rubric of speeds:
  
  - Speed (Slow)= 0 --> This is the start speed and is 500ms per blink of each led.
  - Speed (Medium)= 1 --> Thi speed is 100ms per blink of each led.
  - Speed (High)= 2 --> Thi speed is 10ms per blink of each led.
  - Speed (Back to slow mode)= 3 --> Thi speed value of speed, returns to the slow mode of the beginning. 

When the User Button was pushed, the speed state incremented by one to the next state. 
When the letter 's' is sent by the UART serial console, the speed state is incremented by one to the next state. 

  